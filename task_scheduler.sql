-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2022 at 08:30 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task_scheduler`
--

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE `boards` (
  `boardID` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `dashboardID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `boards`
--

INSERT INTO `boards` (`boardID`, `title`, `description`, `isDeleted`, `dashboardID`) VALUES
(1, 'Development', NULL, 0, 1),
(2, 'UI UX', NULL, 0, 2),
(3, 'Designer', NULL, 0, 2),
(4, 'Daily Quest', NULL, 0, 5),
(5, 'board 1.1', NULL, 0, 6),
(6, 'board 1.2', NULL, 0, 6),
(7, 'board 2.1', NULL, 0, 7),
(8, 'board 2.2', NULL, 0, 7);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `commentID` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `content` varchar(255) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `timestampCreated` datetime NOT NULL,
  `userID` int(11) NOT NULL,
  `taskID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`commentID`, `title`, `content`, `isDeleted`, `timestampCreated`, `userID`, `taskID`) VALUES
(1, 'This Is Report from Employee', 'hello this is content of comment, use for employee report.', 0, '2022-02-24 06:20:35', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `dashboards`
--

CREATE TABLE `dashboards` (
  `dashboardID` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `workspaceID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dashboards`
--

INSERT INTO `dashboards` (`dashboardID`, `title`, `description`, `isDeleted`, `workspaceID`) VALUES
(1, 'Database Dev', NULL, 0, 1),
(2, 'UI Dev', NULL, 0, 1),
(5, 'halo dunia', NULL, 0, 2),
(6, 'dashboard 1', NULL, 0, 3),
(7, 'dashboard 2', NULL, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `fileID` int(11) NOT NULL,
  `fileName` varchar(60) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `fullpath` varchar(255) NOT NULL,
  `section` varchar(60) NOT NULL,
  `referenceID` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`fileID`, `fileName`, `extension`, `fullpath`, `section`, `referenceID`, `description`) VALUES
(1, 'Test Content', 'html', 'index.html', 'tasks.content', 1, NULL),
(2, 'Test Content', 'html', 'index.html', 'tasks.content', 2, NULL),
(3, 'attachment for comment', 'html', 'index.html', 'comments.attachments', 1, 'attachment for comments'),
(4, 'photo', 'jpg', 'photo.jpg', 'user_details.photo', 1, NULL),
(5, 'attachment for content', 'html', 'index.html', 'tasks.attachments', 3, 'attachment for tasks'),
(6, 'Test Content', 'html', 'index.html', 'tasks.content', 2, NULL),
(7, 'Test Content', 'html', 'index.html', 'tasks.content', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `logID` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `userID` int(11) NOT NULL,
  `description` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`logID`, `timestamp`, `userID`, `description`) VALUES
(1, '2022-12-31 10:42:21', 1, 'Test log 1 - Uploaded photo'),
(2, '2022-03-10 19:19:33', 1, 'Delte - users : Update WBmxD'),
(3, '2022-03-10 21:33:11', 1, 'Create - 5 : undefined'),
(4, '2022-03-10 21:35:03', 1, 'Create - LNOXz : undefined'),
(5, '2022-03-10 21:35:56', 1, 'Create - gB9RN : undefined'),
(6, '2022-03-10 21:43:55', 1, 'Create - workspaces : 5B3ez'),
(7, '2022-03-10 21:47:33', 1, 'Create - dashboards : QBpQN'),
(8, '2022-03-10 21:53:19', 1, 'Create - boards : gB9RN'),
(9, '2022-03-10 22:15:08', 1, 'Update - roles : aBxLB'),
(10, '2022-03-10 22:15:08', 1, 'Update - priorities : QBpQN'),
(11, '2022-03-10 22:18:16', 1, 'Update - tags : gB9RN'),
(12, '2022-03-10 22:18:16', 1, 'Update - workspaces : 5B3ez'),
(13, '2022-03-10 22:24:13', 1, 'Update - dashboards : QBpQN'),
(14, '2022-03-10 23:42:28', 1, 'Delete - priorities : QBpQN'),
(15, '2022-03-10 23:42:28', 1, 'Delete - tags : gB9RN'),
(16, '2022-03-10 23:53:57', 1, 'Delete - dashboards : QBpQN'),
(17, '2022-03-10 23:53:57', 1, 'Delete - dashboards : ADjMz'),
(18, '2022-03-10 23:55:34', 1, 'Delete - dashboards : QBpQN'),
(19, '2022-03-10 23:55:34', 1, 'Delete - dashboards : ADjMz'),
(20, '2022-03-11 00:05:33', 1, 'Delete - dashboards : QBpQN'),
(21, '2022-03-11 00:05:33', 1, 'Delete - dashboards : ADjMz'),
(22, '2022-03-11 00:05:33', 1, 'Delete - dashboards : QBpQN'),
(23, '2022-03-11 00:06:30', 1, 'Delete - dashboards : QBpQN'),
(24, '2022-03-11 00:06:30', 1, 'Delete - dashboards : ADjMz'),
(25, '2022-03-11 00:06:30', 1, 'Delete - dashboards : QBpQN'),
(26, '2022-03-11 00:07:25', 1, 'Delete - dashboards : QBpQN'),
(27, '2022-03-11 00:07:25', 1, 'Delete - dashboards : ADjMz'),
(28, '2022-03-11 00:07:25', 1, 'Delete - boards : QBpQN'),
(29, '2022-03-11 00:10:22', 1, 'Delete - dashboards : kzXRB'),
(30, '2022-03-11 00:10:22', 1, 'Delete - dashboards : jD4XD'),
(31, '2022-03-11 00:10:22', 1, 'Delete - boards : WBmxD'),
(32, '2022-03-11 00:13:30', 1, 'Delete - dashboards : kzXRB'),
(33, '2022-03-11 00:13:30', 1, 'Delete - dashboards : jD4XD'),
(34, '2022-03-11 00:13:30', 1, 'Delete - boards : WBmxD'),
(35, '2022-03-11 00:14:57', 1, 'Delete - dashboards : kzXRB'),
(36, '2022-03-11 00:14:57', 1, 'Delete - dashboards : jD4XD'),
(37, '2022-03-11 00:14:57', 1, 'Delete - boards : WBmxD'),
(38, '2022-03-11 00:20:32', 1, 'Delete - tasks : kzXRB'),
(39, '2022-03-11 00:20:32', 1, 'Delete - tasks : jD4XD'),
(40, '2022-03-11 00:20:32', 1, 'Delete - boards : WBmxD'),
(41, '2022-03-11 00:20:32', 1, 'Delete - tasks : VNAZz'),
(42, '2022-03-11 00:20:32', 1, 'Delete - tasks : dDRkB'),
(43, '2022-03-11 00:20:32', 1, 'Delete - tasks : kzXRB'),
(44, '2022-03-11 00:20:32', 1, 'Delete - tasks : jD4XD'),
(45, '2022-03-11 00:20:32', 1, 'Delete - boards : aNgnB'),
(46, '2022-03-11 00:20:32', 1, 'Delete - boards : WBmxD'),
(47, '2022-03-11 00:20:32', 1, 'Delete - dashboards : aNgnB'),
(48, '2022-03-11 00:20:32', 1, 'Delete - tasks : QBpQN'),
(49, '2022-03-11 00:20:32', 1, 'Delete - tasks : ADjMz'),
(50, '2022-03-11 00:20:32', 1, 'Delete - tasks : aNgnB'),
(51, '2022-03-11 00:20:32', 1, 'Delete - tasks : WBmxD'),
(52, '2022-03-11 00:20:32', 1, 'Delete - tasks : VNAZz'),
(53, '2022-03-11 00:20:32', 1, 'Delete - tasks : dDRkB'),
(54, '2022-03-11 00:20:32', 1, 'Delete - tasks : kzXRB'),
(55, '2022-03-11 00:20:32', 1, 'Delete - tasks : jD4XD'),
(56, '2022-03-11 00:20:32', 1, 'Delete - boards : QBpQN'),
(57, '2022-03-11 00:20:32', 1, 'Delete - boards : ADjMz'),
(58, '2022-03-11 00:20:32', 1, 'Delete - boards : aNgnB'),
(59, '2022-03-11 00:20:32', 1, 'Delete - boards : WBmxD'),
(60, '2022-03-11 00:20:32', 1, 'Delete - dashboards : ADjMz'),
(61, '2022-03-11 00:20:32', 1, 'Delete - dashboards : aNgnB'),
(62, '2022-03-11 00:20:32', 1, 'Delete - workspaces : LNOXz');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notifID` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `assigneeID` int(11) NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `timestampCreated` datetime NOT NULL,
  `taskID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notifID`, `description`, `assigneeID`, `isActive`, `timestampCreated`, `taskID`) VALUES
(2, 'Administrator Closed Test Content', 1, 0, '2022-02-24 06:11:37', 4);

-- --------------------------------------------------------

--
-- Table structure for table `priorities`
--

CREATE TABLE `priorities` (
  `priorityID` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `description` text DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `priorities`
--

INSERT INTO `priorities` (`priorityID`, `title`, `description`, `isDeleted`) VALUES
(1, 'High', NULL, 0),
(2, 'Medium', NULL, 0),
(3, 'Low', NULL, 0),
(4, 'Common', NULL, 0),
(5, 'B Ajah', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `progresses`
--

CREATE TABLE `progresses` (
  `progressID` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `description` varchar(225) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `progresses`
--

INSERT INTO `progresses` (`progressID`, `title`, `description`, `isDeleted`) VALUES
(1, 'Created', NULL, 0),
(2, 'Open', NULL, 0),
(3, 'In Progress', NULL, 0),
(4, 'Done', NULL, 0),
(5, 'Delete', NULL, 0),
(6, 'Updated', NULL, 0),
(7, 'Deleted Permanent', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `roleID` int(11) NOT NULL,
  `roleName` varchar(25) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`roleID`, `roleName`, `isDeleted`, `description`) VALUES
(1, 'Owner', 0, 'Owner the company, role as admin'),
(2, 'member', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tagID` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tagID`, `title`, `description`, `isDeleted`) VALUES
(1, 'DB', NULL, 0),
(2, 'UI', NULL, 0),
(3, 'Design', NULL, 0),
(4, 'apa nich', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `taskID` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `contentID` int(11) DEFAULT NULL,
  `creatorID` int(11) NOT NULL,
  `timestampCreated` datetime NOT NULL,
  `dueTime` datetime DEFAULT NULL,
  `progressID` int(11) NOT NULL,
  `assigneeID` int(11) NOT NULL,
  `priorityID` int(11) NOT NULL,
  `tagID` int(11) DEFAULT NULL,
  `boardID` int(11) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`taskID`, `title`, `contentID`, `creatorID`, `timestampCreated`, `dueTime`, `progressID`, `assigneeID`, `priorityID`, `tagID`, `boardID`, `isDeleted`) VALUES
(3, 'Test Title Content', 1, 1, '2022-02-24 06:14:12', '2022-02-28 12:14:12', 2, 2, 1, 1, 2, 0),
(4, 'Test Title Content 2', 2, 1, '2022-02-24 06:14:12', NULL, 4, 2, 4, NULL, 3, 0),
(5, 'task 1.1.1', NULL, 1, '2022-03-10 17:34:11', '2022-03-10 17:34:11', 1, 1, 1, NULL, 5, 0),
(6, 'task 1.1.2', NULL, 1, '2022-03-10 17:34:11', '2022-03-10 17:34:11', 1, 1, 1, NULL, 5, 0),
(7, 'task 1.2.1', NULL, 1, '2022-03-10 17:34:11', '2022-03-10 17:34:11', 1, 1, 1, NULL, 6, 0),
(8, 'task 1.2.2', NULL, 1, '2022-03-10 17:34:11', '2022-03-10 17:34:11', 1, 1, 1, NULL, 6, 0),
(9, 'task 2.1.1', NULL, 1, '2022-03-10 17:34:11', '2022-03-10 17:34:11', 1, 1, 1, NULL, 7, 0),
(10, 'task 2.1.2', NULL, 1, '2022-03-10 17:34:11', '2022-03-10 17:34:11', 1, 1, 1, NULL, 7, 0),
(25, 'Test PUT task', 6, 1, '2022-03-11 02:27:30', '2022-12-31 07:00:00', 1, 1, 1, NULL, 1, 0),
(26, 'Test PUT task', 7, 1, '2022-03-11 02:27:30', '2022-12-31 07:00:00', 1, 1, 1, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userID` int(11) NOT NULL,
  `roleID` int(11) NOT NULL,
  `userDetailID` int(11) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `keygen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `roleID`, `userDetailID`, `isDeleted`, `keygen`) VALUES
(1, 1, 3, 0, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImFCeExCIiwiaWF0IjoxNjQ2OTM3ODgyfQ.pRiB_2sVCC5JBs4Yd53Juxc_Br_xBKcJfHflPTCyj8c'),
(2, 2, 4, 0, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVCM2V6IiwiaWF0IjoxNjQ2NzU4ODk3fQ.arslHjVxqO6FLFjOTVAJZ6l8ZAZZDIdcEStdY8JLtMg'),
(8, 1, 10, 1, NULL),
(9, 2, 11, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `userDetailID` int(11) NOT NULL,
  `fullName` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `placeOfBirth` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `photoID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`userDetailID`, `fullName`, `email`, `phone`, `dateOfBirth`, `placeOfBirth`, `password`, `photoID`) VALUES
(3, 'Administrator', 'admin@taskscheduler.com', '+6281380877345', '1995-12-31', 'Earth', '$2a$12$lkpRxgtCbyJZ/skNgpmkcu5aYsoaTKcH2q3J2W73E66hRcIzsMmva', 4),
(4, 'Regular User', 'user@taskscheduler.com', '+6281380877346', '1995-12-31', 'Earth', '$2a$12$YCHkRYoDAixoWk9OI9bTM.nNmXKDIZgUqUOFePftreDsrCFGVWgK2', NULL),
(10, 'Salman Thoriq Al Farisyi', 'salmanthoriq1995@taskscheduler.com', '+6281380877350', '1995-12-31', 'Earth', '$2b$12$byfog25Gpz0LQ2slQGVeuu9Y23tTuU2eyc5bNQ.jI.HMesHrVdEe.', NULL),
(11, 'Fikran Jabbar Pasha', 'fikranjabbar@taskscheduler.com', '+6281380877348', '1995-12-31', 'Earth', '$2b$12$ZfjR5TvtV/skabgBzcnRXeTYJxmSvjra3HvwES2nPH2Or.XmRDacy', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `workspaces`
--

CREATE TABLE `workspaces` (
  `workspaceID` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `workspaces`
--

INSERT INTO `workspaces` (`workspaceID`, `title`, `description`, `isDeleted`) VALUES
(1, 'IT Division', NULL, 0),
(2, 'halo dunia', NULL, 0),
(3, 'Workspace', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boards`
--
ALTER TABLE `boards`
  ADD PRIMARY KEY (`boardID`),
  ADD KEY `boards_fk0` (`dashboardID`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`commentID`);

--
-- Indexes for table `dashboards`
--
ALTER TABLE `dashboards`
  ADD PRIMARY KEY (`dashboardID`),
  ADD KEY `dashboards_fk0` (`workspaceID`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`fileID`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`logID`),
  ADD KEY `logs_fk0` (`userID`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notifID`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`priorityID`);

--
-- Indexes for table `progresses`
--
ALTER TABLE `progresses`
  ADD PRIMARY KEY (`progressID`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`roleID`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tagID`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`taskID`),
  ADD UNIQUE KEY `content` (`contentID`),
  ADD KEY `tasks_fk0` (`creatorID`),
  ADD KEY `tasks_fk1` (`progressID`),
  ADD KEY `tasks_fk2` (`assigneeID`),
  ADD KEY `tasks_fk3` (`priorityID`),
  ADD KEY `tasks_fk4` (`tagID`),
  ADD KEY `tasks_fk5` (`boardID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `userDetailID` (`userDetailID`),
  ADD KEY `users_fk0` (`roleID`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`userDetailID`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `photoID` (`photoID`);

--
-- Indexes for table `workspaces`
--
ALTER TABLE `workspaces`
  ADD PRIMARY KEY (`workspaceID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boards`
--
ALTER TABLE `boards`
  MODIFY `boardID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dashboards`
--
ALTER TABLE `dashboards`
  MODIFY `dashboardID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `fileID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `logID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notifID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `priorityID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `progresses`
--
ALTER TABLE `progresses`
  MODIFY `progressID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `roleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tagID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `taskID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `userDetailID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `workspaces`
--
ALTER TABLE `workspaces`
  MODIFY `workspaceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `boards`
--
ALTER TABLE `boards`
  ADD CONSTRAINT `boards_fk0` FOREIGN KEY (`dashboardID`) REFERENCES `dashboards` (`dashboardID`);

--
-- Constraints for table `dashboards`
--
ALTER TABLE `dashboards`
  ADD CONSTRAINT `dashboards_fk0` FOREIGN KEY (`workspaceID`) REFERENCES `workspaces` (`workspaceID`);

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_fk0` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`);

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_fk0` FOREIGN KEY (`creatorID`) REFERENCES `users` (`userID`),
  ADD CONSTRAINT `tasks_fk1` FOREIGN KEY (`progressID`) REFERENCES `progresses` (`progressID`),
  ADD CONSTRAINT `tasks_fk2` FOREIGN KEY (`assigneeID`) REFERENCES `users` (`userID`),
  ADD CONSTRAINT `tasks_fk3` FOREIGN KEY (`priorityID`) REFERENCES `priorities` (`priorityID`),
  ADD CONSTRAINT `tasks_fk4` FOREIGN KEY (`tagID`) REFERENCES `tags` (`tagID`),
  ADD CONSTRAINT `tasks_fk5` FOREIGN KEY (`boardID`) REFERENCES `boards` (`boardID`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_fk0` FOREIGN KEY (`roleID`) REFERENCES `roles` (`roleID`),
  ADD CONSTRAINT `users_fk1` FOREIGN KEY (`userDetailID`) REFERENCES `user_details` (`userDetailID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
